export const environment = {
  production: true,
  AUTH_API: 'https://lion-music-server.herokuapp.com',
  STORE_API: 'https://lion-music-server.herokuapp.com/music/'
};
