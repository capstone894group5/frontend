import {Song} from './song';

export class Playlist {
  public title: string;
  public songs: Array<Song>;

  constructor(title: string) {
    this.title = title;
    this.songs = [];
  }
}
