export class Song {
  readonly title: string;
  streamURL: string;
  artistName: string;
  albumTitle: string;
  albumOrderNumber: number;
  displayName: string;

  constructor(title: string) {
    this.title = title;
  }
}
