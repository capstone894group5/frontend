import { Song } from './song';

export class Album {
  readonly title: string;
  public artistName: string;
  public songs: Array<Song>;

  constructor(title: string) {
    this.title = title;
  }
}
