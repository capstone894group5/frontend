import {Album} from './album';

export class Artist {
  public albums: Array<Album>;
  readonly name: string;

  constructor(name: string) {
    this.name = name;
    this.albums = [];
  }

}
