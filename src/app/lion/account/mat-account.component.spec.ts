import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatAccountComponent } from './mat-account.component';

describe('AccountComponent', () => {
  let component: MatAccountComponent;
  let fixture: ComponentFixture<MatAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
