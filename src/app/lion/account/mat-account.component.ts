import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { UserService } from "../../services/user.service";
import { MatDialog } from "@angular/material/dialog";
import { PasswordDialogComponent } from "./password-dialog/password-dialog.component";
import {EmailDialogComponent} from "./email-dialog/email-dialog.component";

@Component({
  selector: 'app-m-account',
  templateUrl: './mat-account.component.html',
  styleUrls: ['./mat-account.component.scss']
})
export class MatAccountComponent implements OnInit {
  constructor(
    private authService: AuthService,
    public user: UserService,
    private dialog: MatDialog
  ) { }

  passwordChange() {
    const dialogRef = this.dialog.open(PasswordDialogComponent);
    dialogRef.afterClosed().toPromise().then(result => {
      if (result !== undefined) {
        // const currentPassword = result[0];
        // const newPassword = result[1];
        // const newPasswordConfirm = result[2];
        // if (newPassword === newPasswordConfirm) {
        //   this.authService.login(this.user.name, currentPassword).subscribe((response) => {
        //     if (response.status === 200) {
        //       console.log('User password confirmed, proceed with change:');
        //       this.authService.updatePassword(this.user.name, newPassword).subscribe(response => {
        //         if (response.status === 200) {
        //           alert('Password successfully changed!');
        //         }
        //       });
        //     } else {
        //       alert('Current password incorrect.');
        //     }
        //   });
        // }
        alert('Feature disabled in demo mode.');
      }
    });
  }

  emailChange() {
    const dialogRef = this.dialog.open(EmailDialogComponent);
    dialogRef.afterClosed().toPromise().then(result => {
      if (result !== undefined) {
        // const currentPassword = result[0];
        // const newEmail = result[1];
        // this.authService.login(this.user.name, currentPassword).subscribe((response) => {
        //   if (response.status === 200) {
        //     console.log('User email confirmed, proceed with change:');
        //     this.authService.updateEmail(this.user.name, newEmail).subscribe((response) => {
        //       if (response.status === 200) {
        //         alert('Email successfully changed!');
        //       }
        //     });
        //   } else {
        //     alert('Current password incorrect.');
        //   }
        // });
        alert('Feature disabled in demo mode.');
      }
    });
  }

  ngOnInit() {
  }

}
