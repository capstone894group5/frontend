import {Component, OnInit, ViewChild, Output, EventEmitter, Input} from '@angular/core';
import {UserService} from '../../services/user.service';
import {MatMenuTrigger} from '@angular/material';
import {Song} from '../../model/song';
import {MaterialPlayerComponent} from '../player/material-player.component';
import {Playlist} from '../../model/playlist';
import {MatDialog} from '@angular/material';
import {PlaylistDialogComponent} from './playlist-dialog/playlist-dialog.component';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
  providers: [MaterialPlayerComponent]
})
export class PlaylistComponent implements OnInit {
  @ViewChild(MatMenuTrigger, {static: true}) matMenuTrigger: MatMenuTrigger;

  @Output()
  songClicked = new EventEmitter<any>();

  @Output()
  playlistPlayed = new EventEmitter<Playlist>();

  @Output()
  playlistShuffled = new EventEmitter<Playlist>();

  @Input()
  playerIndex;

  playlists;

  makeNewPlaylist() {
    const dialogRef = this.dialog.open(PlaylistDialogComponent);
    dialogRef.componentInstance.title = 'Create New Playlist'
    dialogRef.afterClosed().toPromise().then(result => {
      if (result !== undefined) {
        this.user.createPlaylist(result);
      }
    });
  }

  play(playlist: Playlist) {
    this.playlistPlayed.emit(playlist);
  }

  shuffle(playlist: Playlist) {
    this.playlistShuffled.emit(playlist);
  }

  rename(playlist: Playlist) {
    const dialogRef = this.dialog.open(PlaylistDialogComponent);
    dialogRef.componentInstance.title = 'Rename Playlist'
    dialogRef.afterClosed().toPromise().then(result => {
      if (result !== undefined) {
        this.user.renamePlaylist(playlist.title, result);
      }
    });
  }

  removeSong(playlist: Playlist, song: Song) {
    this.user.removeSongFromPlaylist(playlist.title, song);
  }

  delete(playlist: Playlist) {
    this.user.deletePlaylist(playlist.title);
  }

  sendSong(song: Song, playlist: Playlist) {
    this.songClicked.emit({song, playlist});
  }

  constructor(
    public user: UserService,
    public player: MaterialPlayerComponent,
    private dialog: MatDialog
  ) {
    this.playlists = this.user.playlists;
  }

  ngOnInit() {
  }

  openEditPlaylistMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.matMenuTrigger.openMenu();
  }

  closeEditPlaylistMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.matMenuTrigger.closeMenu();
  }

  openEditSongMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.matMenuTrigger.openMenu();
  }

  closeEditSongMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.matMenuTrigger.closeMenu();
  }

}
