import {Song} from '../../model/song';

export interface Track {
  url: string;
  index: number;
}
