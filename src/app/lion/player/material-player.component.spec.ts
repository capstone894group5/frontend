import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialPlayerComponent } from './material-player.component';

describe('MaterialPlayerComponent', () => {
  let component: MaterialPlayerComponent;
  let fixture: ComponentFixture<MaterialPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
