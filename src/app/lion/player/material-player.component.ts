import { Component, EventEmitter, Input, Output} from '@angular/core';
import { AudioStreamingService } from '../../services/audio-streaming.service';
import { StreamState } from '../../model/stream-state';
import { UserService } from '../../services/user.service';
import { GAEventsService } from '../../services/g-a-events.service';

@Component({
  selector: 'app-material-player',
  templateUrl: './material-player.component.html',
  styleUrls: ['./material-player.component.scss']
})
export class MaterialPlayerComponent {
  state: StreamState;

  @Output()
  nextSongClicked = new EventEmitter();

  @Output()
  previousSongClicked = new EventEmitter();

  @Input()
  firstSongPlaying;

  @Input()
  lastSongPlaying;

  @Input()
  displayMessage;

  constructor(private audioService: AudioStreamingService,
              public user: UserService,
              public analytics: GAEventsService) {
    this.audioService.getState().subscribe(state => {
      this.state = state;
    });
  }

  playStream(url) {
    this.analytics.emitEvent('song', 'play', url);
    this.audioService.playStream(url)
      .subscribe(() => {});
  }

  openFile(url) {
    this.audioService.stop();
    const urlWithIDParam = url + '?id=' + this.user.name;
    this.playStream(urlWithIDParam);
  }

  pause() {
    this.audioService.pause();
  }

  play() {
    this.audioService.play();
  }

  stop() {
    this.audioService.stop();
  }

  next() {
    this.nextSongClicked.emit();
  }

  previous() {
    this.previousSongClicked.emit();
  }

  onSliderChangeEnd(change) {
    this.audioService.seekTo(change.value);
  }

}
