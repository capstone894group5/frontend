import {AfterViewInit, ApplicationRef, Component, ComponentFactoryResolver, Injector, Input, OnInit, ViewChild} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {MaterialPlayerComponent} from '../player/material-player.component';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {StoreComponent} from '../store/store.component';
import {StoreService} from '../../services/store.service';
import {Song} from '../../model/song';
import {Playlist} from '../../model/playlist';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @ViewChild(MaterialPlayerComponent, {static: false}) player: MaterialPlayerComponent;
  @ViewChild(StoreComponent, {static: false}) storeFront: StoreComponent;

  selectedComponent: string;
  currentSongList: Array<Song>;
  currentSong: Song;
  currentSongIsFirst: boolean;
  currentSongIsLast: boolean;
  currentTitle: string;
  currentSongIndexForStore: number;
  currentSongIndexForLibrary: number;
  currentSongIndexForPlaylist: number;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private user: UserService,
    private store: StoreService,
    private router: Router
  ) { }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  isFirstPlaying() {
    return this.currentSongList.indexOf(this.currentSong) === 0;
  }


  isLastPlaying() {
    return this.currentSongList.indexOf(this.currentSong) === this.currentSongList.length - 1;
  }


  playSongOnPlayer(source: string) {
    this.currentSongIndexForStore = this.currentSongList.indexOf(this.currentSong);
    this.currentSongIndexForLibrary = this.currentSongList.indexOf(this.currentSong);
    this.currentSongIndexForPlaylist = this.currentSongList.indexOf(this.currentSong);
    this.currentSongIsFirst = this.isFirstPlaying();
    this.currentSongIsLast = this.isLastPlaying();
    this.currentTitle = this.currentSong.displayName + ' - ' + this.currentSong.artistName;
    this.player.openFile(this.currentSong.streamURL);
  }


  playerRequestedNextSong() {
    if (!this.isLastPlaying()) {
      this.currentSong = this.currentSongList[this.currentSongList.indexOf(this.currentSong) + 1];
      this.playSongOnPlayer('player');
    }
  }


  playerRequestedPreviousSong() {
    if (!this.isFirstPlaying()) {
      this.currentSong = this.currentSongList[this.currentSongList.indexOf(this.currentSong) - 1];
      this.playSongOnPlayer('player');
    }
  }


  storeSentSongToPlayer(song) {
    if (this.currentSongList !== this.store.storePlaylist) {
      this.currentSongList = this.store.storePlaylist;
    }
    this.currentSong = song;
    this.playSongOnPlayer('store');
  }


  userRemovedSong(song: Song) {
    this.user.removeSongFromLibrary(song);
  }


  playlistSentSongToPlayer(container) {
    const playlistSongs = this.user.getPlaylistByName(container.playlist.title).songs
    if (this.currentSongList !== playlistSongs) {
      this.currentSongList = playlistSongs;
    }
    this.currentSong = container.song;
    this.playSongOnPlayer('playlist');
  }


  playlistSentPlaylistToPlayer(playlist: Playlist) {
    const song = playlist.songs[0];
    this.playlistSentSongToPlayer({song, playlist});
  }


  playlistSentShuffledPlaylistToPlayer(playlist: Playlist) {
    const shuffledPlaylist = new Playlist(playlist.title);
    const songs = [...playlist.songs];
    let currentIndex = songs.length;
    let temporaryValue: Song;
    let randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = songs[currentIndex];
      songs[currentIndex] = songs[randomIndex];
      songs[randomIndex] = temporaryValue;
    }
    shuffledPlaylist.songs = songs;
    const song: Song = shuffledPlaylist.songs[0];
    this.playlistSentSongToPlayer({song, playlist: shuffledPlaylist});
  }

  homeSentSongToPlayer(song) {
    const songURL = song.streamURL;
    this.store.storePlaylist.forEach(storeSong => {
      if (storeSong.streamURL === songURL) {
        this.storeSentSongToPlayer(storeSong);
        return;
      }
    });
  }

  librarySentSongToPlayer(song) {
    if (this.currentSongList !== this.user.library) {
      this.currentSongList = this.user.library;
    }
    this.currentSong = song;
    this.playSongOnPlayer('library');
  }

  selectComponent(component) {
    this.user.saveUserUpdatesToServer();
    this.selectedComponent = component;
  }

  navigateToPlaylistPage() {
    this.selectComponent('playlist');
  }

  navigateToStorePage() {
    this.selectComponent('store');
  }

  logout() {
    this.player.stop();
    if (this.user.library !== undefined) {
      this.user.saveUserUpdatesToServer();
    }
    this.router.navigate(['/']).then(() => {
      console.log('user logged out');
    });
  }

  ngOnInit() {
    this.selectedComponent = 'index';
  }


}
