import {Component, OnInit, ViewChild, AfterViewInit, ViewChildren, Output, EventEmitter, Input} from '@angular/core';
import { StoreService } from '../../services/store.service';
import {MaterialPlayerComponent} from '../player/material-player.component';
import {MatMenuTrigger} from '@angular/material';
import {Song} from '../../model/song';
import {UserService} from '../../services/user.service';


@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
  providers: [ MaterialPlayerComponent ]
})
export class StoreComponent implements OnInit, AfterViewInit {
  @ViewChild('songMenuTrigger', {static: true})
  songMenuTrigger: MatMenuTrigger;

  @ViewChild('playlistMenuTrigger', {static: true})
  playlistMenuTrigger: MatMenuTrigger;

  @Output()
  songClicked = new EventEmitter<Song>();

  @Input()
  playerIndex;

  @Output()
  playlistCreation = new EventEmitter<any>();

  sendSong(song: Song) {
    this.songClicked.emit(song);
  }

  songIsSaved(song): boolean {
    return this.user.libraryContainsSong(song);
  }

  toggleSavedSong(song: Song, event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (this.songIsSaved(song)) {
      this.user.removeSongFromLibrary(song);
      return;
    }
    if (!this.songIsSaved(song)) {
      this.user.addSongToLibrary(song);
      return;
    }
  }

  constructor(
    public store: StoreService,
    public user: UserService,
    public player: MaterialPlayerComponent
    ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  navigateToPlaylistPage() {
    this.playlistCreation.emit();
  }


  openSongMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    //  this.songMenuTrigger.openMenu();
  }

  openPlaylistMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.playlistMenuTrigger.openMenu();
  }

  closePlaylistMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.playlistMenuTrigger.closeMenu();
    // this.songMenuTrigger.closeMenu();
  }

}


