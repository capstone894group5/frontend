import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatLibraryComponent } from './mat-library.component';

describe('LibraryComponent', () => {
  let component: MatLibraryComponent;
  let fixture: ComponentFixture<MatLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatLibraryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
