import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {MaterialPlayerComponent} from '../player/material-player.component';
import {StoreService} from '../../services/store.service';
import {UserService} from '../../services/user.service';
import {Song} from '../../model/song';
import {MatMenuTrigger} from '@angular/material';

@Component({
  selector: 'app-m-library',
  templateUrl: './mat-library.component.html',
  styleUrls: ['./mat-library.component.scss'],
  providers: [ MaterialPlayerComponent ]
})
export class MatLibraryComponent implements OnInit {
  @ViewChild('songMenuTrigger', {static: true})
  songMenuTrigger: MatMenuTrigger;

  @ViewChild('playlistMenuTrigger', {static: true})
  playlistMenuTrigger: MatMenuTrigger;

  @Output()
  songClicked = new EventEmitter<Song>();

  @Output()
  songRemovedFromLibrary = new EventEmitter<Song>();

  @Output()
  playlistCreation = new EventEmitter<any>();

  @Input()
  userLibrary;

  @Input()
  playerIndex;

  sendSong(song: Song) {
    this.songClicked.emit(song);
  }

  removeSong(song: Song) {
    this.songRemovedFromLibrary.emit(song);
  }

  constructor(
    public user: UserService,
    public player: MaterialPlayerComponent,
  ) {}

  ngOnInit() {
  }

  openSongMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    //  this.songMenuTrigger.openMenu();
  }

  openPlaylistMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.playlistMenuTrigger.openMenu();
  }

  closePlaylistMenu(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    // this.playlistMenuTrigger.closeMenu();
    // this.songMenuTrigger.closeMenu();
  }

  navigateToPlaylistPage() {
    this.playlistCreation.emit();
  }

}
