import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatHomeComponent } from './mat-home.component';

describe('HomeComponent', () => {
  let component: MatHomeComponent;
  let fixture: ComponentFixture<MatHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
