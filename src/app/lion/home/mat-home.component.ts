import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService } from "../../services/user.service";
import {Song} from "../../model/song";

@Component({
  selector: 'app-m-home',
  templateUrl: './mat-home.component.html',
  styleUrls: ['./mat-home.component.scss']
})
export class MatHomeComponent implements OnInit {

  @Output()
  storeNavigation = new EventEmitter<any>();

  @Output()
  songClicked = new EventEmitter<Song>();

  constructor(public user: UserService) {
  }

  sendSong(song: Song) {
    this.songClicked.emit(song);
  }

  saveSong(song: Song) {

  }

  ngOnInit() {
  }

  navToStore() {
    this.storeNavigation.emit();
  }

}
