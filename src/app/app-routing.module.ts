import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './site/index/index.component';
import { LoginComponent } from './site/login/login.component';
import { AdminComponent } from './site/admin/admin.component';
import { RegistrationComponent } from './site/register/registration.component';
import { AppContainerComponent } from './lion/container/app-container.component';

const routes: Routes = [
  {
    path: '', component: IndexComponent
  },
  {
    path: 'lion', component: AppContainerComponent
  },
  // {
  //   path: 'login', component: LoginComponent
  // },
  // {
  //   path: 'admin', component: AdminComponent
  // },
  // {
  //   path: 'register', component: RegistrationComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
