import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-account',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  private IsMatch: boolean;
  constructor(private authService: AuthService, private router: Router) { }

  username: string;
  firstNameText: string;
  lastNameText: string;
  email: string;
  addressText: string;
  stateText: string;
  zipText: string;
  password: string;
  confirmationText: string;

  ngOnInit() {
  }

  submit() {
    if (this.password.valueOf() === this.confirmationText.valueOf()) {
      this.authService.register(this.username, this.password, this.email).subscribe((response) => {
        if (response.status === 200) {
          alert('Account registered! Please log in.');
          this.router.navigate(['/login']);
        } else {
          alert('Account could not be registered with provided information.');
        }
      });
    } else {
     this.confirmationText = 'Passwords Do Not Match';
    }
  }

}
