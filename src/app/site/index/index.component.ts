import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {UserService} from "../../services/user.service";
import {StoreService} from "../../services/store.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  demoName = "mark";
  demoPass = "admin";
  loading = false;

  constructor(private authService: AuthService,
              private user: UserService,
              private store: StoreService,
              private router: Router
  ) { }

  loadDemoApplication() {
    this.loading = true;
    this.authService.login(this.demoName, this.demoPass).subscribe((response) => {
      if (response.status === 200) {
        this.user.loadUserFromServer(this.demoName);
        this.router.navigate(['/lion']);
      } else {
        alert('Backend may be down.');
      }
    });
  }

  ngOnInit() {
  }

}
