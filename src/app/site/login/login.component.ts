import { Component, OnInit, ViewEncapsulation, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {UserService} from '../../services/user.service';
import {StoreService} from '../../services/store.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  userNameText: string;
  passwordText: string;

  constructor(
    private elementRef: ElementRef,
    private authService: AuthService,
    private user: UserService,
    private store: StoreService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  submit() {
    this.authService.login(this.userNameText, this.passwordText).subscribe((response) => {
      console.log(response.status);
      if (response.status === 200) {
        this.user.loadUserFromServer(this.userNameText);
        this.router.navigate(['/lion']);
      } else if (response.status === 401) {
        alert('Account not found. Please try again.');
      }
    });
  }


}
