import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AuthService } from './services/auth.service';

import { RegistrationComponent } from './site/register/registration.component';
import { AdminComponent } from './site/admin/admin.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './site/login/login.component';
import { IndexComponent } from './site/index/index.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatInputModule,
    MatMenuModule,
    MatSidenavModule,
    MatSliderModule,
    MatTableModule,
    MatToolbarModule
} from '@angular/material';
import { PlaylistComponent} from './lion/playlist/playlist.component';
import { ReactiveFormsModule} from '@angular/forms';
import { SidebarComponent } from './lion/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppContainerComponent } from './lion/container/app-container.component';
import { MaterialPlayerComponent } from './lion/player/material-player.component';
import { StoreComponent } from './lion/store/store.component';
import { MatLibraryComponent } from './lion/library/mat-library.component';
import { MatHomeComponent } from './lion/home/mat-home.component';
import { MatAccountComponent } from './lion/account/mat-account.component';
import { FormsModule } from '@angular/forms';
import { UserService } from './services/user.service';
import { StoreService } from './services/store.service';
import { PlaylistDialogComponent } from './lion/playlist/playlist-dialog/playlist-dialog.component';
import { GAEventsService } from './services/g-a-events.service';
import { PasswordDialogComponent } from './lion/account/password-dialog/password-dialog.component';
import { EmailDialogComponent } from './lion/account/email-dialog/email-dialog.component';
import { EraseDialogComponent } from './lion/account/erase-dialog/erase-dialog.component';
import { MatGridListModule } from "@angular/material/grid-list";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    IndexComponent,
    RegistrationComponent,
    PlaylistComponent,
    SidebarComponent,
    AppContainerComponent,
    MaterialPlayerComponent,
    StoreComponent,
    MatLibraryComponent,
    MatHomeComponent,
    MatAccountComponent,
    PlaylistDialogComponent,
    PasswordDialogComponent,
    EmailDialogComponent,
    EraseDialogComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    MatCheckboxModule,
    MatInputModule,
    MatTableModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatIconModule,
    MatListModule,
    MatSliderModule,
    FormsModule,
    MatExpansionModule,
    MatMenuModule,
    MatDialogModule,
    MatCardModule,
    MatGridListModule
  ],
  exports: [
    CommonModule, MatInputModule, MatTableModule, MatToolbarModule, ReactiveFormsModule
  ],
  providers: [AuthService, UserService, StoreService, GAEventsService],
  bootstrap: [AppComponent],
  entryComponents: [PlaylistDialogComponent, PasswordDialogComponent, EmailDialogComponent, EraseDialogComponent]
})

export class AppModule {}
