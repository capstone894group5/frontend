import { TestBed } from '@angular/core/testing';

import { AudioStreamingService } from './audio-streaming.service';

describe('AudioStreamingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AudioStreamingService = TestBed.get(AudioStreamingService);
    expect(service).toBeTruthy();
  });
});
