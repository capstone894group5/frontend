import { TestBed } from '@angular/core/testing';

import { GAEventsService } from './g-a-events.service';

describe('GAEventsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GAEventsService = TestBed.get(GAEventsService);
    expect(service).toBeTruthy();
  });
});
