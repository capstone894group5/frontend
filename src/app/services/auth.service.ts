import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http: HttpClient) { }

  login(username, password) {
    const credentials = {
      username,
      password
    };
    return this.http.post(`${environment.AUTH_API}/login`, credentials, {observe: 'response'} );
  }

  register(username, password, email) {
    const credentials = {
      username,
      password,
      email
    };
    return this.http.post(`${environment.AUTH_API}/register`, credentials, {observe: 'response'} );
  }

  updatePassword(username: string, password: string) {
    return this.http.post(`${environment.AUTH_API}/passwordChange`, {username, password}, {observe: 'response'} );
  }

  updateEmail(username: string, email: string) {
    return this.http.post(`${environment.AUTH_API}/emailChange`, {username, email}, {observe: 'response'} );
  }

  getLibrary(id: string) {
    return this.http.get(`${environment.AUTH_API}/library`, {params: {id}, observe: 'response'}).toPromise();
  }

  getProfile(username: string) {
    return this.http.post(`${environment.AUTH_API}/profile`, {username}, {observe: 'response'}).toPromise();
  }

  updateLibrary(id: string, content: any) {
    return this.http.post(`${environment.AUTH_API}/library`, {id, content}, {observe: 'response'}).toPromise();
  }

}
