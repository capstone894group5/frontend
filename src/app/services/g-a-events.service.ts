import { Injectable } from '@angular/core';
// @ts-ignore
import {} from '@types/google.analytics';

@Injectable({
  providedIn: 'root'
})
export class GAEventsService {

  public emitEvent(eventCategory: string,
                   eventAction: string,
                   eventLabel: string = null,
                   eventValue: number = null) {
    ga('send', 'event', { eventCategory, eventLabel, eventAction, eventValue });
  }

  constructor() { }
}
