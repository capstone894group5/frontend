import {Injectable} from '@angular/core';
import {Artist} from '../model/artist';
import {HttpClient} from '@angular/common/http';
import {Album} from '../model/album';
import {Song} from '../model/song';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private http: HttpClient) {
    this.loadStoreFromServer();
  }

  artists = [];
  storePlaylist = [];


  private static alphaSort(a: string , b: string): number {
    if (a.toUpperCase() < b.toUpperCase()) {
      return -1;
    }
    if (a.toUpperCase() > b.toUpperCase()) {
      return 1;
    }
    return 0;
  }


  private static numSort(x: number, y: number): number {
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }
    return 0;
  }


  private loadStoreFromServer() {
    this.fetchArtists().then(() => {
      this.artists = this.artists.sort( (a: Artist, b: Artist) => {
        return StoreService.alphaSort(a.name, b.name);
      });
      this.artists.forEach(artist => {
        this.fetchAlbums(artist).then(() => {
          artist.albums.sort( (a: Album, b: Album) => {
            return StoreService.alphaSort(a.title, b.title);
          });
          artist.albums.forEach(album => {
            this.fetchSongs(album).then(() => {
              this.processSongs(album);
            });
          });
        });
      });
      this.createStorePlaylist();
    });
  }

  private createStorePlaylist() {
    this.artists.forEach(artist => {
      artist.albums.forEach(album => {
        album.songs.forEach(song => {
          this.storePlaylist.push(song);
        });
      });
    });
  }

  private fetchArtists(): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = environment.STORE_API;
      this.http.get<any[]>(url).toPromise()
        .then((res: any) => {
            this.artists = res.map(data => {
              return new Artist(data['Name']);
            });
            resolve();
          },
          err => {
            reject(err);
          }
        );
    });
  }

  private fetchAlbums(artist: Artist) {
    return new Promise((resolve, reject) => {
      const url = environment.STORE_API + artist.name;
      this.http.get<any[]>(url).toPromise()
        .then((res: any) => {
            artist.albums = res.map(data => {
              const newAlbum = new Album(data['Name']);
              newAlbum.artistName = artist.name;
              return newAlbum;
            });
            resolve();
          },
          err => {
            reject(err);
          }
        );
    });
  }

  private fetchSongs(album: Album) {
    return new Promise((resolve, reject) => {
      const url = environment.STORE_API + album.artistName + '/' + album.title;
      this.http.get<any[]>(url).toPromise()
        .then((res: any) => {
            album.songs = res.map(data => {
              return new Song(data['Name']);
            });
            resolve();
          },
          err => {
            reject(err);
          }
        );
    });
  }

  private processSongs(album: Album) {
    album.songs.forEach(song => {
      song.streamURL = environment.STORE_API + album.artistName + '/' + album.title + '/' + song.title;
      song.artistName = album.artistName;
      song.albumTitle = album.title;
      song.albumOrderNumber = Number(song.title.substr(0, 2 ));
      song.displayName = song.title.substring(song.title.indexOf('-') + 1, song.title.lastIndexOf('.'));
    });
    album.songs.sort((a, b) => {
      return StoreService.numSort(a.albumOrderNumber, b.albumOrderNumber);
    });
    album.songs.forEach(song => {
      this.storePlaylist.push(song);
    });
  }


}
