import {EventEmitter, Injectable, Output} from '@angular/core';
import {Song} from '../model/song';
import {Playlist} from '../model/playlist';
import {AuthService} from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public auth: AuthService) {}

  name: string;
  email: string;
  libraryID: string;
  library: Array<Song>;
  playlists: Array<Playlist>;
  hasRecPlaylist: boolean;
  recommendedPlaylist: Playlist;

  private static buildSongFromJSON(data): Song {
    const song = new Song(data['title']);
    song.streamURL = data['streamURL'];
    song.artistName = data['artistName'];
    song.albumTitle = data['albumTitle'];
    song.albumOrderNumber = data['albumOrderNumber'];
    song.displayName = data['displayName'];
    return song;
  }

  public libraryContainsSong(song: Song): boolean {
    for (const index in this.library) {
      const libSong = this.library[index];
      if (libSong.streamURL === song.streamURL) {
        return true;
      }
    }
    return false;
  }

  public playlistContainsSong(playlist: Playlist, song: Song): boolean {
    for (const index in playlist.songs) {
      const playlistSong = playlist.songs[index];
      if (playlistSong.streamURL === song.streamURL) {
        return true;
      }
    }
    return false;
  }

  private userHasPlaylist(name: string): boolean {
    for (const index in this.playlists) {
      if (this.playlists[index].title === name) {
        return true;
      }
    }
    return false;
  }


  public addSongToLibrary(song: Song): boolean {
    if (!this.libraryContainsSong(song)) {
      this.library.push(song);
      return true;
    }
    return false;
  }


  public removeSongFromLibrary(song: Song): boolean {
    if (this.libraryContainsSong(song)) {
      for (const index in this.library) {
        const libSong = this.library[index];
        if (libSong.streamURL === song.streamURL) {
          this.library.splice(Number(index), 1);
          return true;
        }
      }
    }
    return false;
  }

  public getPlaylistByName(playlistName: string) {
    for (const index in this.playlists) {
      if (this.playlists[index].title === playlistName) {
        return this.playlists[index];
      }
    }
  }


  createPlaylist(playlistName: string): boolean {
    if (!this.userHasPlaylist(playlistName)) {
      const newPlaylist = new Playlist(playlistName);
      newPlaylist.songs = [];
      this.playlists.push(newPlaylist);
      return true;
    }
    return false;
  }

  renamePlaylist(playlistName: string, newName: string) {
    if (!this.userHasPlaylist(newName)) {
      this.getPlaylistByName(playlistName).title = newName;
    }
  }

  deletePlaylist(playlistName: string): boolean {
    if (this.userHasPlaylist(playlistName)) {
      this.playlists.splice(this.playlists.indexOf(this.getPlaylistByName(playlistName)), 1);
      return true;
    }
    return false;
  }


  public addSongToPlaylist(playlistName: string, song: Song): boolean {
    const playlist = this.getPlaylistByName(playlistName);
    if (!this.playlistContainsSong(playlist, song)) {
      playlist.songs.push(song);
      return true;
    }
    return false;
  }


  public removeSongFromPlaylist(playlistName: string, song: Song): boolean {
    const playlist = this.getPlaylistByName(playlistName);
    if (this.playlistContainsSong(playlist, song)) {
      for (const index in playlist.songs) {
        const playlistSong = playlist.songs[index];
        if (playlistSong.streamURL === song.streamURL) {
          playlist.songs.splice(Number(index), 1);
          return true;
        }
      }
      return true;
    }
    return false;
  }


  public loadUserFromServer(userName: string) {
    this.auth.getProfile(userName).then(res => {
      const emailAddress = res.body['email'];
      const libID = res.body['library'];
      if (libID !== '000000000000000000000000') {
        this.name = userName;
        this.email = emailAddress;
        this.libraryID = libID;
        this.library = [];
        this.playlists = [];
        this.auth.getLibrary(libID).then(data => {
          const content = JSON.parse(data.body['content']);
          const songs = content['songs'];
          if (songs !== undefined) {
            for (const index in songs) {
              const songData = songs[index];
              this.library.push(UserService.buildSongFromJSON(songData));
            }
          }
          const playlists = content['playlists'];
          if (playlists !== undefined) {
            for (const index in playlists) {
              const playlistData = playlists[index];
              const newPlaylist = new Playlist(playlistData['title']);
              if (playlistData['songs'][0] !== null) {
                for (const songIndex in playlistData['songs']) {
                  const songData = playlistData['songs'][songIndex];
                  newPlaylist.songs.push(UserService.buildSongFromJSON(songData));
                }
              } else {
                newPlaylist.songs = [];
              }
              if (newPlaylist.title === 'Recommended') {
                this.hasRecPlaylist = true;
                this.recommendedPlaylist = newPlaylist;
              }
              this.playlists.push(newPlaylist);
            }
          }
        });
      }
    });
  }

  public saveUserUpdatesToServer() {
    const payload = {songs: this.library, playlists: this.playlists};
    this.auth.updateLibrary(this.libraryID, JSON.stringify(payload)).then(res => {
      if (res.status === 200) {
        return;
      } else {
        console.log(res.status, res.statusText);
        return;
      }
    });
  }

}
