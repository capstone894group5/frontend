const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('./dist/music-app-login'));

app.get('/*', function(req,res) {
  res.sendFile(path.join(__dirname,'/dist/music-app-login/index.html'));
});

app.listen(process.env.PORT || 8080);
